Hallo en welkom bij de ReadMe van groepje 1 van klas I1, van de opleiding HBO ICT Hogeschool Utrecht.
Als opdracht hebben wij gekozen om de vertrektijden van de treinen vanaf Nederlandse stations en eventuele werkzaamheden en storingen te weergeven in een applicatie.
De applicatie is opgebouwd met de programmeertaal Python met de styling word gedaan met behulp van de Tkinter library.
Ook maken wij gebruik van de library’s XMLtoDict en Request.
Alle informatie word verkregen met behulp van de NS API.

Beginnen:
De bestanden die u nodig heeft zijn (connection.py en vertrektijden.py), deze kunt u downloaden en in uw Python programma zetten. (Zoals PyCharm)
Vervolgens moet u mogelijk nog de XMLtoDict en Request module importeren.
Als u hier niet uit komt, zie de HELP sectie op de Python website.
En hierna moet u als enige nog uw gebruikersnaam en wachtwoord invullen in het bestand connection.py.
U vult bij de variabelen username en password uw gebruikersnaam en wachtwoord in die u van de NS heeft gekregen.

Mogelijkheden:
Wanneer u alle stappen correct heeft uitgevoerd is het mogelijk om de vertrektijden van de treinen vanaf Schiphol in te zien wanneer u op de eerste knop drukt.
Wanneer u op de tweede knop drukt kunt u zelf een station naar keuze invullen (binnen Nederland) en de treintijden hiervan inzien.
De derde knop opent een venster met de storingen die nu aan de gang zijn. 
En de vierde knop opent een venster met alle geplande werkzaamheden binnen op het treinnetwerk.

NS API:
Om toegang te krijgen tot de API en documentatie te vinden over de NS API verwijzen wij u door naar de officiële website. http://www.ns.nl/api/home

===========================================================================
Hello and welcome to the ReadMe of our project, made for an assignment for the Hogeschool Utrecht.
The assignment was to retrieve the departure time information for dutch trains heading out of a station of choice. 
We have chosen to make Schiphol our default station, which is the first button when you start the program.
Further we have the second button, where you can put in a station name yourself and it will show the departure times for the trains heading out of this station.
And we have 2 extra buttons that show’s the current disruptions en planned contractions on the tracks.

Getting started:
To get started you simply download the files connection.py and vertrektijden.py.
You then import the modules XMLtoDict and Request and change the username and password in the connection.py to your own username and password that you received from the NS.

NS API:
For acces to the NS API and the documentation we refer you to the official website website. http://www.ns.nl/api/home