__author__ = 'Dion'
# import tkinter for GUI
from tkinter import *

# import xmltodict to convert xml to readable code
import xmltodict

# import the connect class
from connection import connect_class

# create the master frame
master = Tk()
master.wm_title("Vertrektijden")

class index():
    # Starts immediately when the application is opened
    # Loads the startingscreen with the buttons
    def __init__(self):
        photo = PhotoImage(file="image.gif")
        w = Canvas(master, width=800, height=900, bg='#fc3')
        w.create_image(60, 100, image = photo, anchor = CENTER)
        w.pack(side="top", fill="both", expand=False)
        w.configure(scrollregion=(-400, -400, 400, 400))
        w.pack()
        # Spacings and title
        Label(w, text="", bg='#fc3').grid(row=0,column=0, pady=50, padx=(200, 5))
        Label(w, text="Welkom op station Schiphol!", bg='#fc3', fg='#006', justify=RIGHT).grid(row=0, column=1, columnspan=2)
        Label(w, text="", bg='#fc3').grid(row=0,column=5, pady=0, padx=(5, 200))
        Label(w, text="", bg='#fc3').grid(row=1,column=0, pady=60)
        # Get the amount of constructions/disruptions in the current service and display in the buttons
        ammount_con = self.check_constructions()
        constructions = 'Werkzaamheden \n(%s)' % ammount_con
        ammount_dis = self.check_disruptions()
        disruptions = 'Storingen \n(%s)' % ammount_dis
        Button(w, text='Sluiten', command=master.quit, bg='#006', fg='#FFF').grid(row=0, column=3, pady=20)
        Button(w, text='Vertrektijden\n Huidig\n Station', command=self.show_departure_current_station, width=13,
               height=5, bg='#006', fg='#FFF').grid(row=3, column=1, pady=0, padx=10)
        Button(w, text='Vertrektijden\n Andere\n Stations', command=self.show_departure_time, width=13, height=5,
               bg='#006', fg='#FFF').grid(row=3, column=2, pady=0, padx=10)
        Button(w, text=disruptions, command=self.show_disruptions, width=13, height=5, bg='#006', fg='#FFF').grid(row=3,
                                                                                                                  column=3,
                                                                                                                  pady=0,
                                                                                                                  padx=10)
        Button(w, text=constructions, command=self.show_constructions, width=13, height=5, bg='#006', fg='#FFF').grid(
            row=3, column=4, pady=0, padx=10)
        Label(w, text="", bg='#fc3').grid(row=4,column=0, pady=50)

        master.mainloop()

    # Shows all departure times of the trains departing from Schiphol
    # For the best experience select the station that you want to see here
    def show_departure_current_station(self):
        global current_station
        response = connected.connection('http://webservices.ns.nl/ns-api-avt?station=SHL')
        # Check for multiple results and show these
        try:
            popup = Toplevel()
            popup.title("Vertrektijden Schiphol")
            stations_dict = xmltodict.parse(response.text)
            Label(popup, text="De rode regels geven aan dat er een spoorwijziging is.", bg="#59E817").pack()
            scrollbar = Scrollbar(popup)
            scrollbar.pack(side=RIGHT, fill=Y)
            listbox = Listbox(popup, width=50, height=10, yscrollcommand=scrollbar.set)
            y = 0
            #while there are results
            for i in stations_dict['ActueleVertrekTijden']['VertrekkendeTrein']:
                try:
                    ritnummer = i['VertrekSpoor']['#text']
                except:
                    ritnummer = '?'
                try:
                    wijziging_vertrekspoor = i['VertrekSpoor']['@wijziging']
                except:
                    wijziging_vertrekspoor = 'false'
                try:
                    eindbestemming = i['EindBestemming']
                except:
                    eindbestemming = 'niet bekend'
                try:
                    vertrektijd = i['VertrekTijd'][11:19]
                except:
                    vertrektijd = 'niet bekend'
                sting = "{0:<6} - {1:2} - {2:<8}".format(vertrektijd, ritnummer, eindbestemming)
                listbox.insert(END, sting)
                if wijziging_vertrekspoor == 'true':
                    listbox.itemconfig(y, {'bg': 'red'})
                y += 1
            listbox.pack(side=LEFT, fill=BOTH)
            scrollbar.config(command=listbox.yview)
        # if either the station isn't found or there is only one train
        except:
            #
            # if there is only one train departing display it
            try:
                popup = Toplevel()
                popup.title("Vertrektijden Schiphol")
                stations_dict = xmltodict.parse(response.text)
                scrollbar = Scrollbar(popup)
                scrollbar.pack(side=RIGHT, fill=Y)
                listbox = Listbox(popup, width=50, height=10, yscrollcommand=scrollbar.set)
                try:
                    ritnummer = i['ActueleVertrekTijden']['VertrekkendeTrein']['VertrekSpoor']['#text']
                except:
                    ritnummer = '?'
                try:
                    wijziging_vertrekspoor = i['ActueleVertrekTijden']['VertrekkendeTrein']['VertrekSpoor']['@wijziging']
                except:
                    wijziging_vertrekspoor = 'false'
                try:
                    eindbestemming = i['ActueleVertrekTijden']['VertrekkendeTrein']['EindBestemming']
                except:
                    eindbestemming = '?'
                try:
                    vertrektijd = i['ActueleVertrekTijden']['VertrekkendeTrein']['VertrekTijd'][11:19]
                except:
                    vertrektijd = '?'
                sting = "{0:<6} - {1:2} - {2:<8}".format(vertrektijd, ritnummer, eindbestemming)
                listbox.insert(END, sting)
                if wijziging_vertrekspoor == 'true':
                    listbox.itemconfig(y, {'bg': 'red'})
                listbox.pack(side=LEFT, fill=BOTH)
                scrollbar.config(command=listbox.yview)
            # if there is no station with that name/invalid station
            except:
                    popup.destroy()
                    self.output['text'] = 'Er is iets fout gegaan!'
    # Shows all departure times of the trains departing a chosen station
    def show_departure_time(self):
        sdt = Toplevel(bg='#fc3')
        sdt.title("Vertrektijd zoeken")
        Label(sdt, text="Voer een station in:", bg='#fc3').grid(row=0)
        self.e1 = Entry(sdt)
        self.e1.grid(row=1)
        self.output = Label(sdt, width=50, height=5, bg='#fc3', fg='#006')
        self.output.grid(row=2)
        Button(sdt, text='Sluiten', command=sdt.destroy, bg='#006', fg='#FFF').grid(row=4, column=0, sticky=W, pady=4)
        #onclick show a popup with all the trains
        Button(sdt, text='Zoeken', command=self.show_departure_fields, fg='#FFF', bg='#006').grid(row=4, column=1,
                                                                                                  sticky=W, pady=4)
    # get all the trains departing from the chosen station
    def show_departure_fields(self):
        # get the station from the input field
        station = self.e1.get()
        response = connected.connection('http://webservices.ns.nl/ns-api-avt?station=' + station)
        # if there are multiple results display them
        try:
            popup = Toplevel()
            popup.title('Vertrektijden ' + station)
            stations_dict = xmltodict.parse(response.text)
            Label(popup, text="De rode regels geven aan dat er een spoorwijziging is.", bg="#59E817").pack()
            scrollbar = Scrollbar(popup)
            scrollbar.pack(side=RIGHT, fill=Y)
            listbox = Listbox(popup, width=50, height=10, yscrollcommand=scrollbar.set)
            y = 0
            z = 0
            for i in stations_dict['ActueleVertrekTijden']['VertrekkendeTrein']:
                try:
                    track = i['VertrekSpoor']['#text']
                except:
                    track = '?'
                    z +=1
                try:
                    changed_departure_platform = i['VertrekSpoor']['@wijziging']
                except:
                    changed_departure_platform = 'false'
                    z+=1
                try:
                    destination = i['EindBestemming']
                except:
                    destination = '?'
                    z+=1
                try:
                    departure_time = i['VertrekTijd'][11:19]
                except:
                    departure_time = '?'
                    z+=1
                if z == 4:
                    print(i['VertrekSpoor']['#text'])
                sting = "{0:<6} - {1:2} - {2:<8}".format(departure_time, track, destination)
                listbox.insert(END, sting)
                if changed_departure_platform == 'true':
                    listbox.itemconfig(y, {'bg': 'red'})
                y += 1
            listbox.pack(side=LEFT, fill=BOTH)
            scrollbar.config(command=listbox.yview)
        # if there is only one or no result
        except:
            # if there is one result show it
            try:
                i = xmltodict.parse(response.text)
                try:
                    track = i['ActueleVertrekTijden']['VertrekkendeTrein']['VertrekSpoor']['#text']
                except:
                    track = '?'
                try:
                    changed_departure_platform = i['ActueleVertrekTijden']['VertrekkendeTrein']['VertrekSpoor']['@wijziging']
                    print(changed_departure_platform)
                except:
                    changed_departure_platform = 'false'
                try:
                    destination = i['ActueleVertrekTijden']['VertrekkendeTrein']['EindBestemming']
                except:
                    destination = '?'
                try:
                    departure_time = i['ActueleVertrekTijden']['VertrekkendeTrein']['VertrekTijd'][11:19]
                except:
                    departure_time = '?'
                sting = "{0:<6} - {1:2} - {2:<8}".format(departure_time, track, destination)
                listbox.insert(END, sting)
                if changed_departure_platform == 'true':
                    listbox.itemconfig(y, {'bg': 'red'})
                listbox.pack(side=LEFT, fill=BOTH)
                scrollbar.config(command=listbox.yview)
            # Display error message
            except:
                popup.destroy()
                self.output['text'] = 'Dit station kan niet gevonden worden!'
    # show all the constructions that passengers may suffer from
    def show_constructions(self):
        response = connected.connection('http://webservices.ns.nl/ns-api-storingen?station=SCHIPHOL&unplanned=true')
        popup = Toplevel(bg='#fc3')
        popup.title("Werkzaamheden")
        constructions_dict = xmltodict.parse(response.text)
        x = 1
        output = Text(popup, bg='#fc3', fg='#006', relief="flat", width=100, height=50, borderwidth=0,
                      highlightthickness=0)
        # if there are multiple constructions show them
        try:
            for i in constructions_dict['Storingen']['Gepland']['Storing']:
                traject = i['Traject']
                try:
                    reden = i['Reden']
                except:
                    reden = "Geen reden opgegeven"
                output.insert(END, traject + "\n")
                output.insert(END, reden + "\n", 'reason')
                output.insert(END, '\n')
                output.grid(row=x)
                x += 1
            output.configure(state="disabled")
            output.tag_configure("reason", background="#FF0000")
            Button(popup, text='Sluiten', command=popup.destroy, bg='#006', fg='#FFF').grid(row=x)
        #if there is one or none
        except:
            # show the construction
            try:
                constructions = constructions_dict['Storingen']['Gepland']['Storing']['Traject']
                reason = constructions_dict['Storingen']['Gepland']['Storing']['Reden']
                output.insert(END, constructions + "\n")
                output.insert(END, reason + "\n", 'reason')
                output.grid(row=1)
                output.configure(state="disabled")
                output.tag_configure("reason", background="#FF0000")
                Button(popup, text='Sluiten', command=popup.destroy, bg='#006', fg='#FFF').grid(row=2)
            # if there are no constructions
            except:
                output = Label(popup, text='Er zijn op dit moment geen storingen.', width=50, height=5)
                output.grid(row=1)
                output.configure(state="disabled")
                Button(popup, text='Sluiten', command=popup.destroy, bg='#006', fg='#FFF').grid(row=2)
    # show all the disruptions that passengers may suffer from
    def show_disruptions(self):
        response = connected.connection('http://webservices.ns.nl/ns-api-storingen?actual=true&unplanned=false')
        popup = Toplevel(bg='#fc3')
        popup.title("Storingen")
        disruptions_dict = xmltodict.parse(response.text)
        x = 1
        output = Text(popup, bg='#fc3', fg='#006', relief="flat", width=100, height=50, borderwidth=0,
                      highlightthickness=0)
        # if there are multiple disruptions show them
        try:
            for i in disruptions_dict['Storingen']['Ongepland']['Storing']:
                traject = i['Traject']
                try:
                    reden = i['Reden']
                except:
                    reden = "Geen reden opgegeven"
                output.insert(END, traject + "\n")
                output.insert(END, reden + "\n", 'reason')
                output.insert(END, '\n')
                output.grid(row=x)
                x += 1
            output.configure(state="disabled")
            output.tag_configure("reason", background="#FF0000")
            Button(popup, text='Sluiten', command=popup.destroy, bg='#006', fg='#FFF').grid(row=x)
        # if there is one or none
        except:
            # if there is one disruption
            try:
                disruption = disruptions_dict['Storingen']['Ongepland']['Storing']['Traject']
                reden = disruptions_dict['Storingen']['Ongepland']['Storing']['Reden']
                output.insert(END, disruption + "\n")
                output.insert(END, reden + '\n', 'reason')
                output.grid(row=1)
                output.configure(state="disabled")
                output.tag_configure("reason", background="#FF0000")
                Button(popup, text='Sluiten', command=popup.destroy, bg='#006', fg='#FFF').grid(row=2)
            # if there are no constructions
            except:
                output = Label(popup, text='Er is iets fout gegaan, probeer het later opnieuw.', width=50, height=5)
                output.grid(row=1)
                output.configure(state="disabled")
                Button(popup, text='Sluiten', command=popup.destroy, bg='#006', fg='#FFF').grid(row=2)

    # convert the station name to a station code
    def station_to_code(self, station):
        global auth_details
        station_lijst = connected.connection('http://webservices.ns.nl/ns-api-stations')
        stations_dict = xmltodict.parse(station_lijst.text)
        #find the station code
        for i in stations_dict['stations']['station']:
            if station == i['name']:
                #return station code
                return i['code']

    # return the amount of constructions in progress and planned
    def check_constructions(self):
        response = connected.connection('http://webservices.ns.nl/ns-api-storingen?station=SCHIPHOL&unplanned=true')
        constructions_dict = xmltodict.parse(response.text)
        x = 0
        try:
            for i in constructions_dict['Storingen']['Gepland']['Storing']:
                trash = i['Traject']
                x += 1
            return x
        except:
            try:
                trash = constructions_dict['Storingen']['Gepland']['Storing']['Traject']
                return 1
            except:
                return 0
    #return the amount of disruptions in progress
    def check_disruptions(self):
        response = connected.connection('http://webservices.ns.nl/ns-api-storingen?actual=true&unplanned=false')
        disruptions_dict = xmltodict.parse(response.text)
        x = 0
        try:
            for i in disruptions_dict['Storingen']['Ongepland']['Storing']:
                trash = i['Traject']
                x += 1
            return x
        except:
            try:
                trash = disruptions_dict['Storingen']['Ongepland']['Storing']['Traject']
                return 1
            except:
                return 0

# call the connection class
connected = connect_class()

# start the index class
x = index()
